//pins
#define PIN_LED 13

int PIN_ENABLE[] = {8, 2};
int PIN_STEP[] = {9, 3};
int PIN_DIR[] = {10, 4};
int PIN_RESET[] = {11, 5};

//actions. these just need to be unique
#define ACTION_CONTINUE 0
#define ACTION_STOP 1
#define ACTION_START 2
#define ACTION_SET_YAW_HZ 3
#define ACTION_SET_PITCH_HZ 4

//states. these just need to be unique
#define STATE_STOPPED 1
#define STATE_STARTED 2

//global control
short action = ACTION_CONTINUE;
short state = STATE_STOPPED;
short param = 0;
int lastTime = millis();

//motor controls: [0] yaw, [1] pitch
int speedHz[] = {0, 0};

boolean stepUp[] = {false, false};
void interruptHandler(int motorIndex) {
  stepUp[motorIndex] = !stepUp[motorIndex];
  if (stepUp[motorIndex]) {
    digitalWrite(PIN_ENABLE[motorIndex], LOW);
    digitalWrite(PIN_STEP[motorIndex], HIGH);
  } else {
    digitalWrite(PIN_STEP[motorIndex], LOW);
    digitalWrite(PIN_ENABLE[motorIndex], HIGH);
  }
}

// ISR for TIMER1 when the CTC (Clear Timer on Compare Match) flag is set with counting value specified in OCR1A
ISR (TIMER1_COMPA_vect) {
  interruptHandler(0);
}

ISR (TIMER2_COMPA_vect) {
  interruptHandler(1);
}

void startedInit() {
  digitalWrite(PIN_ENABLE[0], HIGH);
  digitalWrite(PIN_STEP[0], LOW);
  startTimer(0);
  digitalWrite(PIN_ENABLE[1], HIGH);
  digitalWrite(PIN_STEP[1], LOW);
  startTimer(1);
}

//started state
void started(int elapsedTimeMillis) {
  if (speedHz[0] == 0) {
    digitalWrite(PIN_ENABLE[0], HIGH);
    stopTimer(0);
  }
  if (speedHz[1] == 0) {
    digitalWrite(PIN_ENABLE[1], HIGH);
    stopTimer(1);
  }
  
  digitalWrite(PIN_DIR[0], speedHz[0] < 0 ? LOW : HIGH);
  digitalWrite(PIN_DIR[1], speedHz[1] < 0 ? LOW : HIGH);
}


//stopped state -- do nothing
void stopped(int elapsedTimeMillis) {
  stopTimer(0);
  digitalWrite(PIN_ENABLE[0], HIGH);
  digitalWrite(PIN_STEP[0], LOW);
  stopTimer(1);
  digitalWrite(PIN_ENABLE[1], HIGH);
  digitalWrite(PIN_STEP[1], LOW);
}


//receive and parse input from some control device
#define BLUETOOTH_BUFFER_SIZE 48
char bluetoothBuffer[BLUETOOTH_BUFFER_SIZE] = {0};
short bluetoothBufferPos = 0;

void getInput() {
  action = ACTION_CONTINUE;
  param = 0;
  
  //read commands from bluetooth
  while (Serial.available()) {
      char inChar = (char)Serial.read();
      if (inChar == '\n') {
        bluetoothBuffer[bluetoothBufferPos] = '\0';
        Serial.print("Received command: " );
        Serial.println(bluetoothBuffer);
        
        if (!strncmp(bluetoothBuffer, "start", 5)) {
          action = ACTION_START;
        } else if (!strncmp(bluetoothBuffer, "stop", 4)) {
          action = ACTION_STOP;
        } else if (!strncmp(bluetoothBuffer, "yawhz ", 6)) {
          action = ACTION_SET_YAW_HZ;
          param = atoi(bluetoothBuffer + 6);
        } else if (!strncmp(bluetoothBuffer, "pithz ", 6)) {
          action = ACTION_SET_PITCH_HZ;
          param = atoi(bluetoothBuffer + 6);
        } else {
          Serial.println("Unknown command");
        }
        
        bluetoothBufferPos = 0;
      } else {
        bluetoothBuffer[bluetoothBufferPos++] = inChar;
      }
  }
}


//todo: safe param setting
//performs an action, such as setting parameters or transitioning to states
void doAction() {
  switch (action) {
    case ACTION_CONTINUE:
      break;
    case ACTION_STOP:
      state = STATE_STOPPED;
      break;
    case ACTION_START:
      startedInit();
      state = STATE_STARTED;
      break;
    case ACTION_SET_YAW_HZ:
      digitalWrite(PIN_RESET[1], LOW);
      digitalWrite(PIN_RESET[1], HIGH);
      if (speedHz[1] == 0) {
        speedHz[1] = param;
        startTimer(1);
      } else {
        speedHz[1] = param;
        changeSpeed(1);
      }
      break;
    case ACTION_SET_PITCH_HZ:
      digitalWrite(PIN_RESET[0], LOW);
      digitalWrite(PIN_RESET[0], HIGH);
      if (speedHz[0] == 0) {
        speedHz[0] = param;
        startTimer(0);
      } else {
        speedHz[0] = param;
        changeSpeed(0);
      }
      break;
  }
}


//branches execution based on what state the state machine is in
void runState(int elapsedTimeMillis) {
  switch (state) {
    case STATE_STOPPED:
      stopped(elapsedTimeMillis);
      break;
    case STATE_STARTED:
      started(elapsedTimeMillis);
      break;
  }
}


void loop() {  
  //get user input
  getInput();
  
  //transitions and settings
  doAction();
  
  //run the state
  int now = millis();
  int elapsedTime = now - lastTime;
  lastTime = now;
  runState(elapsedTime);
}


void changeSpeed(int motorIndex) {
  int stepFreqHz = abs(speedHz[motorIndex]);
  // Disable global interrupt  
  cli();
  if (motorIndex == 0) {
    // set compare match register to desired timer count based on stepping frequency
    // Calculation: AT328 has internal clock of 16Mhz
    // Prescaler set to CS10 and CS12 (scale down 1024 times), so timer tick is 16Mhz/1024 = 15625 Hz (15625 ticks in 1 second)
    // If compare match is 15624 (15625 - 1 reset cycle) -> triggered every second. Each trigger is to toggle rising or falling edge -> half cycle.
    // So OCR1A = (15625 - 1) is 2 second per step or 0.5 Hz
    // OCR1A value for stepFreqHz is 15625/(stepFreqHz/0.5) - 1
    if (stepFreqHz != 0){
      OCR1A = int(15625/(stepFreqHz*2)) - 1;
    } else{
      // Invalid Zero frequency, set to default 0.5Hz or 2 sec per step
      OCR1A = 15624; // 2 sec per step or 0.5 Hz
    }
    // reset Timer 1 counter
    TCNT1 = 0;
  } else if (motorIndex == 1) {
    if (stepFreqHz > 30){
      OCR2A = int(15625/(stepFreqHz*2)) - 1;
      Serial.println(stepFreqHz);
      Serial.println(OCR2A);
    } else {
      OCR2A = 254; // 2 sec per step or 0.5 Hz
    }
    // reset Timer 2 counter
    TCNT2 = 0;    
  }
  // Enable global interrupt
  sei();
}


void startTimer(int motorIndex) {
  int stepFreqHz = abs(speedHz[motorIndex]);
  
  cli();          // disable global interrupts
  
  if (motorIndex == 0) {
    // initialize Timer1
    TCCR1A = 0;     // set entire TCCR1A register to 0
    TCCR1B = 0;     // same for TCCR1B
    // set compare match register to desired timer count based on stepping frequency
    // Calculation: AT328 has internal clock of 16Mhz
    // Prescaler set to CS10 and CS12 (scale down 1024 times), so timer tick is 16Mhz/1024 = 15625 Hz (15625 ticks in 1 second)
    // If compare match is 15624 (15625 - 1 reset cycle) -> triggered every second. Each trigger is to toggle rising or falling edge -> half cycle.
    // So OCR1A = (15625 - 1) is 2 second per step or 0.5 Hz
    // OCR1A value for stepFreqHz is 15625/(stepFreqHz/0.5) - 1
    if (stepFreqHz != 0){
      OCR1A = int(15625/(stepFreqHz*2)) - 1;
    } else{
      // Invalid Zero frequency, set to default 0.5Hz or 2 sec per step
      OCR1A = 15624; // 2 sec per step or 0.5 Hz
    }
    
    // turn on CTC mode:
    TCCR1B |= (1 << WGM12);
    // Set CS10 and CS12 bits for 1024 prescaler (Timer1):
    TCCR1B |= (1 << CS10);
    TCCR1B |= (1 << CS12);
    // enable timer compare interrupt:
    TIMSK1 |= (1 << OCIE1A);
    
    TCNT1 = 0;
  } else if (motorIndex == 1) {
    TCCR2A = 0;
    TCCR2B = 0;
    
    if (stepFreqHz > 30){
      OCR2A = int(15625/(stepFreqHz*2)) - 1;
    } else {
      // Invalid Zero frequency, set to default 0.5Hz or 2 sec per step
      OCR2A = 254; // 2 sec per step or 0.5 Hz
    }
    // Set Timer2 to use CTC mode with OCR2A compare and ISR
    TCCR2A |= (1 << WGM21);
    
    // Prescalling to 1024 (Timer2  needs all 3 CS20 CS21 CS22 set)
    TCCR2B |= (1 << CS20);
    TCCR2B |= (1 << CS21);
    TCCR2B |= (1 << CS22);
    // Enable timer2 interupt on OCR2A matching
    TIMSK2 |= (1 << OCIE2A);
    
    TCNT2 = 0;
  }
  
  // enable global interrupts:
  sei();
}

void stopTimer(int motorIndex) {
    cli(); // disable global interrupts
    if (motorIndex == 0) {
      TCCR1A = 0; // set entire TCCR1A register to 0
      TCCR1B = 0; // same for TCCR1B
    } else if (motorIndex == 1) {
      TCCR2A = 0; // set entire TCCR1A register to 0
      TCCR2B = 0; // same for TCCR1B
    }
    sei(); // enable global interrupts:
}

void setupPins(int motorIndex) {
  pinMode(PIN_ENABLE[motorIndex], OUTPUT);
  pinMode(PIN_STEP[motorIndex], OUTPUT);
  pinMode(PIN_DIR[motorIndex], OUTPUT);
  
  // Setup timers for step drivers
  digitalWrite(PIN_ENABLE[motorIndex], HIGH);
  digitalWrite(PIN_RESET[motorIndex], HIGH);
}

void setup() {
  pinMode(PIN_LED, OUTPUT);

  setupPins(0);
  setupPins(1);
    
  //bluetooth...
  Serial.begin(57600);
  delay(150);
  Serial.println("Bluetooth serial initialized");
}

